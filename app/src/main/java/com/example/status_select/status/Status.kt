package com.example.status_select.status

import java.time.OffsetDateTime

data class Status(
    val name: String,
    val imageAsSvg: String,
    val trackRequired: Boolean? = null,
    val endDateTime: OffsetDateTime?
)

data class UserStatus(
    val status: Status,
    val endDateTime: OffsetDateTime? = null
)

fun UserStatus.updateDateTime(newDateTime: OffsetDateTime) : UserStatus {
    return UserStatus(this.status, newDateTime)
}


internal fun UndefinedStatus(): Status = Status(
    name = "Не определен",
    imageAsSvg = "<svg width=\"100%\" height=\"100%\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"> <circle cx=\"24\" cy=\"24\" r=\"24\" fill=\"#C0C0C0\"/> </svg>\n",
    trackRequired = false,
    endDateTime = null
)

internal fun DebugStatusList() = listOf(
    Status(
        name = "На смене",
        imageAsSvg = "<svg width=\"100%\" height=\"100%\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"> <circle cx=\"24\" cy=\"24\" r=\"24\" fill=\"#17C49A\"/></svg>\n",
        trackRequired = true,
        endDateTime = OffsetDateTime.now().plusDays(1).plusHours(12).plusMinutes(13)
    ),
    Status(
        name = "Отпуск",
        imageAsSvg = "<svg width=\"100%\" height=\"100%\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"> <circle cx=\"24\" cy=\"24\" r=\"24\" fill=\"#E0164E\"/></svg>",
        trackRequired = true,
        endDateTime = null
    ),
    Status(
        name = "Выходной",
        imageAsSvg = "<svg width=\"100%\" height=\"100%\" viewBox=\"0 0 48 48\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\"> <circle cx=\"24\" cy=\"24\" r=\"24\" fill=\"#5BCDE7\"/></svg>",
        trackRequired = false,
        endDateTime = OffsetDateTime.now().plusHours(12).plusMinutes(13)
    ),
)