package com.example.status_select.status

import android.accounts.Account
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import java.util.*

class StatusViewModel(/** account: Account*/) : ViewModel() {

    // Get from db by ACCOUNT
    private val _statusesList: MutableStateFlow<List<Status>> = MutableStateFlow(DebugStatusList())
    val statusesList: StateFlow<List<Status>> = _statusesList.asStateFlow()

    // Get from db by ACCOUNT
    private val _selectedStatus: MutableStateFlow<UserStatus> = MutableStateFlow(
        UserStatus(
            UndefinedStatus()
        )
    )
    val selectedStatus: StateFlow<UserStatus> = _selectedStatus.asStateFlow()

    private val _viewState: MutableStateFlow<StatusViewState> = MutableStateFlow(StatusViewState.Error)
    val viewState: StateFlow<StatusViewState> = _viewState.asStateFlow()

    fun selectStatus(status: Status) {
        viewModelScope.launch {
            _viewState.emit(StatusViewState.StatusChange)
            delay(1000)
            println("Select status!")
            _selectedStatus.emit(UserStatus(status = status, endDateTime = OffsetDateTime.now()))
            // ?
            _viewState.emit(StatusViewState.Loaded)
        }
    }

    fun setEndDateTime(dateTime: OffsetDateTime) {
        viewModelScope.launch {
            _viewState.emit(StatusViewState.ValidUntilChange)
            delay(1000)
            _selectedStatus.emit(_selectedStatus.value.updateDateTime(dateTime))
            // ?
            _viewState.emit(StatusViewState.Loaded)
        }
    }

    /**
     * Factory of [StatusViewModel].
     *
     * @param [account] The account to which the order belongs
     */
    class Factory(
        private val account: Account
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            require(modelClass == StatusViewModel::class.java) {
                "factory supports StatusViewModel only."
            }
            @Suppress("UNCHECKED_CAST")
            return StatusViewModel(
                // account
            ) as T
        }
    }
}