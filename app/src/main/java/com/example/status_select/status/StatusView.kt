package com.example.status_select.status

import android.graphics.drawable.Drawable
import android.graphics.drawable.PictureDrawable
import androidx.compose.foundation.background
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.caverock.androidsvg.SVG
import com.example.status_select.R
import com.google.accompanist.drawablepainter.rememberDrawablePainter
import java.time.OffsetDateTime
import kotlin.math.roundToInt

@Composable
fun StatusSelectionView(
    // account: Account,
    // statuses: List<Status>,
    viewModel: StatusViewModel = viewModel() // by factory
) {
    val selectedStatus by viewModel.selectedStatus.collectAsState()
    val statusesList by viewModel.statusesList.collectAsState()

    val currentViewState by viewModel.viewState.collectAsState()

    var isDateTimePickerEnabled by rememberSaveable { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        StatusSelectList(
            selectedStatus = selectedStatus.status,
            statusList = statusesList,
            onStatusSelect = { status -> viewModel.selectStatus(status) },
            statusFieldState = currentViewState.fieldStates.statusFieldState
        )

        if (selectedStatus.status.trackRequired != null) {
            StatusSelectionBlockMessage(
                icon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_feedback_24),
                        contentDescription = null,
                        tint = MaterialTheme.colors.primary,
                        modifier = Modifier.padding(start = 12.dp, end = 6.dp, top = 12.dp, bottom = 12.dp)
                    )
                },
                text = {
                    Text(
                        text = stringResource(
                            id = if (selectedStatus.status.trackRequired!!) R.string.status_view_track_collecting_warning_text
                            else R.string.status_view_no_track_collecting_warning_text
                        ),
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier.padding(start = 6.dp, end = 12.dp, top = 12.dp, bottom = 12.dp)
                    )
                }
            )
        }

        StatusUntilValid(
            untilValidValue = selectedStatus.endDateTime,
            statusFieldState = currentViewState.fieldStates.untilValidFieldState,
            onUntilValidClick = { isDateTimePickerEnabled = true }
        )

        if (currentViewState is StatusViewState.Error) {
            StatusSelectionBlockMessage(
                icon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_no_signal_24),
                        contentDescription = null,
                        tint = MaterialTheme.colors.error,
                        modifier = Modifier.padding(start = 12.dp, end = 6.dp, top = 12.dp, bottom = 12.dp)
                    )
                },
                text = {
                    Text(
                        text = stringResource(id = R.string.status_view_no_network_error_text),
                        style = MaterialTheme.typography.caption,
                        color = MaterialTheme.colors.error,
                        modifier = Modifier.padding(start = 6.dp, end = 12.dp, top = 12.dp, bottom = 12.dp)
                    )
                }
            )
        }
    }

    if (isDateTimePickerEnabled) {
        DateTimeBottomSheetSelection(
            currentValidValue = selectedStatus.endDateTime!!,
            onSetDateTimeClick = { selectedDateTime ->
                isDateTimePickerEnabled = false
                viewModel.setEndDateTime(selectedDateTime)
            }
        )
    }

}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun StatusSelectList(
    selectedStatus: Status,
    statusList: List<Status>,
    onStatusSelect: (Status) -> Unit,
    statusFieldState: StatusFieldState
) {
    var expanded by remember { mutableStateOf(false) }

    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = { },
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 6.dp)
    ) {

        OutlinedTextField(
            value = selectedStatus.name,
            onValueChange = { },
            label = { Text(text = stringResource(id = R.string.status_view_status_field_label)) },
            enabled = true, //statusFieldState is StatusFieldState.Enabled,
            readOnly = true,
            modifier = Modifier
                .fillMaxWidth(),
            leadingIcon = {
                Icon(painter = rememberDrawablePainter(drawable = selectedStatus.ConvertImageToDrawable()), contentDescription = null)
            },
            trailingIcon = {
                when (statusFieldState) {
                    is StatusFieldState.Loading -> {
                        CircularProgressIndicator(color = MaterialTheme.colors.primary)
                    }

                    else -> {
                        ExposedDropdownMenuDefaults.TrailingIcon(
                            expanded = expanded,
                            onIconClick = { expanded = !expanded })
                    }
                }
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                backgroundColor = MaterialTheme.colors.secondary,
                focusedBorderColor = MaterialTheme.colors.primary,
                trailingIconColor = MaterialTheme.colors.onSecondary,
            )
        )

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.exposedDropdownSize(true)
        )
        {
            statusList.forEach { status ->
                DropdownMenuItem(onClick = {
                    println("CLICK!")
                    expanded = false
                    onStatusSelect(status)
                }, contentPadding = PaddingValues(start = 0.dp), enabled = true) {
                    TextField(
                        value = status.name,
                        onValueChange = { },
                        readOnly = true,
                        leadingIcon = {
                              Icon(painter = rememberDrawablePainter(drawable = status.ConvertImageToDrawable()), contentDescription = null)
                        },
                        modifier = Modifier.fillMaxWidth(),
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            unfocusedIndicatorColor = Color.Transparent,
                            disabledIndicatorColor = Color.Transparent
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun StatusUntilValid(
    untilValidValue: OffsetDateTime?,
    statusFieldState: StatusFieldState,
    onUntilValidClick: () -> Unit
) {

}


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DateTimeBottomSheetSelection(
    currentValidValue: OffsetDateTime,
    onSetDateTimeClick: (OffsetDateTime) -> Unit
) = ModalBottomSheetLayout(sheetContent = {

}) {

}


@Composable
fun StatusSelectionBlockMessage(
    icon: @Composable () -> Unit,
    text: @Composable () -> Unit
) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 16.dp, vertical = 6.dp)
        .background(color = Color.LightGray, shape = RoundedCornerShape(4.dp)),
        verticalAlignment = Alignment.CenterVertically
    ) {
        icon()
        text()
    }
}


private val DEFAULT_SVG_SIZE = 24f to 24f

fun Status.ConvertImageToDrawable(): Drawable {
    val svg = SVG.getFromInputStream(imageAsSvg.byteInputStream())
    val (svgWidth, svgHeight) = svg
        .documentViewBox?.run { (width() to height()) }
        ?: DEFAULT_SVG_SIZE

    val svgPicture = svg.renderToPicture(svgWidth.roundToInt(), svgHeight.roundToInt())

    return PictureDrawable(svgPicture)
}