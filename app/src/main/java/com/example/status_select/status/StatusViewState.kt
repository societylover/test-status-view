package com.example.status_select.status

sealed class StatusViewState(val fieldStates: StatusFieldsStates) {
    object Error : StatusViewState(StatusFieldsStates(statusFieldState = StatusFieldState.Disabled, untilValidFieldState = StatusFieldState.Disabled))
    object Undefined: StatusViewState(StatusFieldsStates(statusFieldState = StatusFieldState.Enabled, untilValidFieldState = StatusFieldState.Disabled))
    object StatusChange : StatusViewState(StatusFieldsStates(statusFieldState = StatusFieldState.Loading, untilValidFieldState = StatusFieldState.Disabled))
    object ValidUntilChange : StatusViewState(StatusFieldsStates(statusFieldState = StatusFieldState.Disabled, untilValidFieldState = StatusFieldState.Loading))
    object Loaded : StatusViewState(StatusFieldsStates(statusFieldState = StatusFieldState.Enabled, untilValidFieldState = StatusFieldState.Enabled))
    object Initial : StatusViewState(StatusFieldsStates(statusFieldState = StatusFieldState.Disabled, untilValidFieldState = StatusFieldState.Disabled))
}

sealed class StatusFieldState {
    object Loading : StatusFieldState()
    object Enabled : StatusFieldState()
    object Disabled : StatusFieldState()
}

data class StatusFieldsStates(
    val statusFieldState : StatusFieldState,
    val untilValidFieldState : StatusFieldState
)