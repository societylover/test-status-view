package com.example.status_select.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import ru.snrd.orders.ui.theme.Shapes
import ru.snrd.orders.ui.theme.Typography

private val DarkColorPalette = darkColors(

)

private val LightColorPalette = lightColors(
    primary = Color(0xFF28509b),
    primaryVariant = Color(0xFF00296c),
    secondary = Color(0xFFFFFFFF),
    secondaryVariant = Color(0xFFCCCCCC),
    background = Color(0xFFFAFAFA),
    error = Color(0xFFFF1450),
    onError = Color(0xFFFFFFFF),
    onPrimary = Color(0xFFFFFFFF),
    onSecondary = Color(0xFF000000)
)

/**
 * Material Design theme.
 */
@Composable
fun OrdersTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes
    ) {
        content()
    }
}